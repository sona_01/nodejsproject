const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')

const userSchema = new mongoose.Schema({
    name: 
    {
        type: String,
        required:true,
        trim:true
    },
    course:
    {
        type: String,
        required:true,
        trim:true
    },
    email: 
    {
        type:String,
        required:true,
        unique:true,
        trim:true,
        lowercase:true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('Invalid email')
            }
        }
    },
    password:
    {
        type:String,
        trim:true,
        minlength:7,
        required:true,
        validate(value){
            if(value.toLowerCase().includes('password')){
                throw new Error('Password does contain password')
            }
        }

    },
    age: 
    {
        type: Number,
        default:0,
        validate(value){
            if(value<0){
                throw new Error('Age must be a positive number')
            }
        }
    }
})


userSchema.statics.findByCredentials = async (email, password) => {
    const user = await User.findOne({ email })

    if (!user) {
        throw new Error('Unable to login')
    }

    const isMatch = await bcrypt.compare(password, user.password)

    if (!isMatch) {
        throw new Error('Unable to login')
    }

    return user
}


//hash before saving the password
userSchema.pre('save', async function(next){
     const user = this

     if(user.isModified('password')){
         user.password = await bcrypt.hash(user.password, 8)  
     }
     next()
})

const User = mongoose.model('User',userSchema)

module.exports = User




// //Curd Operation
// const mongoose = require('mongoose')
// const validator = require('validator')


// const User = mongoose.model('User', {
//     name: 
//     {
//         type: String,
//         required:true,
//         trim:true
//     },
//     course:
//     {
//         type: String,
//         required:true,
//         trim:true
//     },
//     email: 
//     {
//         type:String,
//         required:true,
//         trim:true,
//         lowercase:true,
//         validate(value){
//             if(!validator.isEmail(value)){
//                 throw new Error('Invalid email')
//             }
//         }
//     },
//     password:
//     {
//         type:String,
//         trim:true,
//         minlength:7,
//         required:true,
//         validate(value){
//             if(value.toLowerCase().includes('password')){
//                 throw new Error('Password does contain password')
//             }
//         }

//     },
//     age: 
//     {
//         type: Number,
//         default:0,
//         validate(value){
//             if(value<0){
//                 throw new Error('Age must be a positive number')
//             }
//         }
//     }
// })

// module.exports = User