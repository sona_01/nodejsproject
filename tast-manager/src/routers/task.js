const express = require('express')
const Tast = require('../models/task')
const router = new express.Router()

router.post('/tasks', (req, res) => {
    const task = new Tast(req.body)

    task.save().then(() => {
        res.status(201).send(task)
    }).catch((e) => {
        res.status(400).send(e);

    })
})

router.get('/tasks', (req, res) => {
    Tast.find({}).then((tasks) => {
        res.status(200).send(tasks)
    }).catch(() => {
    })
})

router.get('/tasks/:id', (req, res) => {
    console.log(req.params)
    const _id = req.params.id

    Tast.findById(_id).then((task) => {
        if (!task) {
            return res.status(404).send()
        }

        res.send(task)
    }).catch((e) => {
        res.status(404).send()
    })
})

module.exports = router




// // Curd Operation
// const express = require('express')
// const Tast = require('../models/task')
// const router = new express.Router()

// router.post('/tasks', (req, res) => {
//     const task = new Tast(req.body)

//     task.save().then(() => {
//         res.status(201).send(task)
//     }).catch((e) => {
//         res.status(400).send(e);

//     })
// })

// router.get('/tasks', (req, res) => {
//     Tast.find({}).then((tasks) => {
//         res.status(200).send(tasks)
//     }).catch(() => {
//     })
// })

// router.get('/tasks/:id', (req, res) => {
//     console.log(req.params)
//     const _id = req.params.id

//     Tast.findById(_id).then((task) => {
//         if (!task) {
//             return res.status(404).send()
//         }

//         res.send(task)
//     }).catch((e) => {
//         res.status(404).send()
//     })
// })

// module.exports = router