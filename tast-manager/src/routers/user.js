const express = require('express')
const User = require('../models/user')
const router = new express.Router()

router.post('/users', (req, res) => {
    //     console.log(req.body)
    //   res.send('Testing')
    const user = new User(req.body)
    user.save().then(() => {
        res.send(user)
    }).catch((e) => {
        //    statuses.com ex-200/201
        res.status(400).send(e)
    })
})

router.post('/users/login', async (req,res)=>{
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password)
        res.send(user)

    }catch (e){
     res.status(400).send
    }
})


router.get('/users', (req, res) => {
    User.find({}).then((users) => {
        res.send(users)
    }).catch((e) => {
        res.status(500).send(e)
    })
})


router.get('/users/:id', (req, res) => {
    console.log(req.params)
    const _id = req.params.id

    User.findById(_id).then((user) => {
        if (!user) {
            return res.status(404).send()
        }

        res.send(user)
    }).catch((e) => {
        res.status(500).send()
    })
})

router.patch('/users/:id', (req, res) => {
    const _id = req.params.id
    const u = req.body
   User.findByIdAndUpdate(_id, u, {
        new: true,
        runValidators: true
    }).then((user) => {

        if (!user) {
            return res.status(404).send()
        }
        res.send(user)
    }).catch((e) => {
        res.status(500).send()
    })
})


router.delete('/users/:id',(req, res) => {
   console.log("Deleted" +req.params)
    const _id = req.params.id

    User.findByIdAndDelete(_id).then((user) => {
        if (!user) {
            return res.status(404).send()
        }

        res.send(user)
    }).catch((e) => {
        res.status(500).send()
    })
})

module.exports= router



// // Curd operation
// const express = require('express')
// const User = require('../models/user')
// const router = new express.Router()

// router.post('/users', (req, res) => {
//     //     console.log(req.body)
//     //   res.send('Testing')
//     const user = new User(req.body)
//     user.save().then(() => {
//         res.send(user)
//     }).catch((e) => {
//         //    statuses.com ex-200/201
//         res.status(400).send(e)
//     })
// })

// router.get('/users', (req, res) => {
//     User.find({}).then((users) => {
//         res.send(users)
//     }).catch((e) => {
//         res.status(500).send(e)
//     })
// })


// router.get('/users/:id', (req, res) => {
//     console.log(req.params)
//     const _id = req.params.id

//     User.findById(_id).then((user) => {
//         if (!user) {
//             return res.status(404).send()
//         }

//         res.send(user)
//     }).catch((e) => {
//         res.status(500).send()
//     })
// })

// router.patch('/users/:id', (req, res) => {
//     const _id = req.params.id
//     const u = req.body

//     User.findByIdAndUpdate(_id, u, {
//         new: true,
//         runValidators: true
//     }).then((user) => {

//         if (!user) {
//             return res.status(404).send()
//         }
//         res.send(user)
//     }).catch((e) => {
//         res.status(500).send()
//     })
// })


// router.delete('/users/:id',(req, res) => {
//    console.log("Deleted" +req.params)
//     const _id = req.params.id

//     User.findByIdAndDelete(_id).then((user) => {
//         if (!user) {
//             return res.status(404).send()
//         }

//         res.send(user)
//     }).catch((e) => {
//         res.status(500).send()
//     })
// })

// module.exports= router