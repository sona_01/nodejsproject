const mongoose = require('mongoose')

mongoose.connect('mongodb://127.0.0.1:27017/task-manager-api', {
    useNewUrlParser: true,
    useCreateIndex: true
})






// //With the Help of required we can get what we want according to our need ex- if we want access only name then we can required 
// const User = mongoose.model('User', {
//     name: {
//         type: String,
//         required:true,
//         trim:true
//     },
//     email: {
//         type:String,
//         required:true,
//         trim:true,
//         lowercase:true,
//         validate(value){
//             if(!validator.isEmail(value)){
//                 throw new Error('Invalid email')
//             }
//         }
//     },
//     password:{
//         type:String,
//         trim:true,
//         minlength:7,
//         required:true,
//         validate(value){
//             if(value.toLowerCase().includes('password')){
//                 throw new Error('Password does contain password')
//             }
//         }

//     },
//     age: {
//         type: Number,
//         default:0,
//         validate(value){
//             if(value<0){
//                 throw new Error('Age must be a positive number')
//             }
//         }
//     }
// })

// // const me = new User({
// //     name: 'Sonam    ',
// //     email:'   SONAM@GMAIL.COM   ',
// //     password: 'sona@123'
// // })
// // me.save().then(() => {
// //     console.log(me)
// // }).catch((error) => {
// //     console.log('Error!', error)
// // })

// //Create Mongoose Task model
// //create Task
// const Task = mongoose.model('Task',{
//     description:{
//      type: String,
//      required:true,
//      trim:true   
//     },
//     completed:{
//         type: Boolean,
//         default:false
//     }
// })

// //new Instance
// const task = new Task({
//     description:'Learn About Mongoose',
//     // completed: false
// })

// //Save
// task.save().then(()=>{
//     console.log(task)
// }).catch((error)=>{
//     console.log(error)
// })