const chalk = require("chalk")
const notes= require("./notes");
const yargs = require("yargs");


// const result =  getNotes();
// console.log(result);

//Chalk package
// const msg= chalk.red.inverse.bold("Hey Beautiful..");
// console.log(msg);



//// user input
// const command = process.argv[2];

// if(command==='sonam')
// {
//     console.log("new node added");
// }
// else if(command==='remove')
// {
//     console.log('node is removed');
// }

//version
//
yargs.version('1.0.0');
// console.log(process.argv);
// console.log(yargs.argv);

//Create add command
yargs.command({
    command:'add',
    describe:'Add a new node',
    builder:{
        title:{
            describe: 'note title',
            demandOption: true,
            title:'string'
        },
        body:{
            describe:'note body',
            demandOption:true,
            title:'string'
        }
    },
    handler: function (argv)
    {
    //    // console.log('Adding new node', argv);
    //    console.log('Title:' +argv.title);
    //    console.log('Body:' +argv.body);
    //adding note
        notes.addNote(argv.title, argv.body)
    }
})

//Create list command
yargs.command({
    command:'list',
    describe:'Add a new list',
    handler(){
        notes.listNote()
    }
})

//Create remove command
yargs.command({
    command:'remove',
    describe:'remove this list',
    builder:{
    title:{
        describe:'new note',
        demandOption:true,
        type: 'string'
    }
    },
    handler: function (argv)
    {
        notes.removeNote(argv.title);
    }
})

//Create read command
yargs.command({
    command:'read',
    describe:'read this list',
    builder:{
        title:{
            describe:'new note',
            demandOption:true,
            type: 'string'
        }
        },
        handler(argv)
        {
            notes.readNote(argv.title);
        }
})
//yargs.argv;
yargs.parse();

