const fs = require('fs');
const chalk = require('chalk')

const getNotes = () => {
    console.log("hello from nodejs...")
}


// add node
   const addNote = function (title, body) {
    const notes = loadNotes();
    // const duplicateArray = notes.filter((note)=> note.title===title)
    const duplicateArray = notes.find((note)=> note.title===title)
    //Without arrow function
    // const duplicateArray = notes.filter(function (note)
    // { return note.title === title
    // })
    
    if(!duplicateArray)
    {
        notes.push({
            title:title,
            body:body
        })
        saveNotes(notes);
        console.log('new node added!!!')
    }else {
        console.log('already take!!!')
    }
   }
// remove node

const removeNote  = function(title)
{
    const notes = loadNotes()
    const datatokeep = notes.filter(function (note){
        return note.title!==title;
    })
    if(notes.length > datatokeep.length)
    {
        console.log(chalk.green.inverse('Note removed'))
        saveNotes(datatokeep);
    }else {
        console.log(chalk.red.inverse('No note Found'))
    }
}

//lising notes
const listNote =()=>{

    const notes = loadNotes()
    console.log(chalk.red.inverse('Your notes'))
    notes.forEach((note)=> {
      console.log(note.title);
    })
}

const readNote = (title)=>
{
const notes = loadNotes()
const note = notes.find((note) => note.title===title)

if(note)
{
    console.log(chalk.inverse(note.title))
    console.log(note.body)
}else{
   console.log(chalk.red.inverse('Note not found'))
}
} 

const saveNotes = function(notes)
{
    const datajson = JSON.stringify(notes)
    fs.writeFileSync('notes.json', datajson);
}
const loadNotes = function () {
    try {
        const dataBuffer = fs.readFileSync('notes.json')
        const datajson = dataBuffer.toString()
        return JSON.parse(datajson);

    } catch (e) {
        return [];
}}
module.exports = {
    getNotes: getNotes,
    addNote: addNote,
    removeNote:removeNote,
    listNote:listNote,
    readNote:readNote
}