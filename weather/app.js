// console.log('shopping');

// setTimeout(()=> {
//     console.log('2 second timer')
// },2000)
// setTimeout(()=> {
//     console.log('0 second timer')
// },0)

// console.log('marketing');

//HTTP Server=> darksky.net/dev

const request = require('request');
const geocode = require('./utils/geocode')
const forecast = require('./utils/forecast')

const address = process.argv[2];

if(!address){
  console.log('please provide an address')
}else{
//Callback Chaining
geocode(address, (error,{latitude, longitude, location})=>{
  if(error){
     return console.log(error);
  }
  
  forecast(latitude, longitude, (error, forecastData) => {
    if(error){
     return console.log(error);
    }
    console.log(location)
    console.log(forecastData)
  })
})
}
console.log(process.argv);
// const url = 'https://api.darksky.net/forecast/ddcd30781ca14fcb652cd02b10f59f02/37.8267,-122.4233?lang=es'

// request({url:url}, (error, response) => {
// //   console.log(response);
// const dataObject = JSON.parse(response.body);
// console.log(data.currently.temperature);
// console.log('It is currently' + dataObject.currently.temperature+'Degrees out. There is '
// +dataObject.currently.precipProbability+'% of chance of rain')
// })

// request({url:url, JSON: true}, (error, response) => {
//     //   console.log(response);
//     const dataObject = JSON.parse(response.body);
//    // console.log(data.currently.temperature);
//     console.log(dataObject.daily.data[0].summary +'It is currently' + dataObject.currently.temperature+'Degrees out. There is '
//     +dataObject.currently.precipProbability+'% of chance of rain')
//     })

   
   
//     //Geocoding
//     //address lat/long=> weather
//    const geoCoding ='https://api.mapbox.com/geocoding/v5/mapbox.places/Los%20Angeles.json?access_token=pk.eyJ1Ijoic29uYTAxIiwiYSI6ImNrN24zdWZyMzA4a3AzZWp3dGk3bndsZHQifQ.vR3vaEeMGTX93xY1ho9vqg&limit=1'

//     request({url:geoCoding,JSON:true},(error,response) =>{
//         const dataObject = JSON.parse(response.body);
//         const latitude = dataObject.features[0].center[1]
//         const long = dataObject.features[0].center[0]
//         console.log(latitude,long)
//     });


// // error handling without using internet
// request({url:url, JSON: true}, (error, response) => {
//     //   console.log(response);
//  if(error)
//  {
//   console.log('internet is not able to connected')
//  }
//  else{
//     const dataObject = JSON.parse(response.body);
//     // console.log(data.currently.temperature);
//      console.log(dataObject.daily.data[0].summary +'It is currently' + dataObject.currently.temperature+'Degrees out. There is '
//      +dataObject.currently.precipProbability+'% of chance of rain')
//  }
// })



// geocode('india', (error,data)=>{
//   console.log('error', error)
//   console.log('data' ,data)
// })



// //Callback abstraction
// // Goal: Create a reusable function for getting the forecast
// //
// // 1. Setup the "forecast" function in utils/forecast.js
// // 2. Require the function in app.js and call it as shown below
// // 3. The forecast function should have three potential calls to callback:
// //    - Low level error, pass string for error
// //    - Coordinate error, pass string for error
// //    - Success, pass forecast string for data (same format as from before)
// forecast(-75.7088, 44.1545, (error, data) => {
//   console.log('Error', error)
//   console.log('Data', data)
// })



