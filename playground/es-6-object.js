//Object property shorthand

const name= 'sonam';
const userAge=20;

const user={
    name,
    age:userAge,
    location:'India'
};
console.log(user);

// object destructing

const product ={
    label:'red notebook',
    price:3,
    stock:201,
    saleprice:undefined
}

// const label = product.label
// const price = product.label

//const {label, price} = product;
//rename
// const {label:productlabel, price,rating=5} = product;
// console.log(productlabel);
// console.log(price);
// console.log(rating);

// with function

const transaction = (type, {label, stock})=>{
   console.log(type, label, stock)
}
transaction('order',product)

